# -*- coding: UTF-8 -*-
from ChampionshipFileParser import ChampionshipFileParser
import re
from datetime import datetime
from DatabaseConnector import get_inscriptions_db
from ConversorTiempos import ConversorTiempos


class LegacyChampioshipFileParser(ChampionshipFileParser):
    def __init__(self, file_path, championship_date):
        super(LegacyChampioshipFileParser, self).__init__(file_path)
        self.event_re = re.compile(
            '\s*(?P<event_number>\d+)\s+((-\s)?)+(?P<relay>\dx)?(?P<distance>\d+)(\s+m\.)?\s+(?P<style>\w+) (?P<gender>\w+)$')
        self.participant_re = re.compile(
            '\s*(?P<initial_position>\d+)\s+(?P<license>\S+)\s+(?P<name>(\S+ )+)\s*(?P<year>\+?\d+)\s+(?P<club>(\S+ )+)\s*(?P<inscription_time>\:?(\d{1,2}:)?\d{1,2}\.\d{1,2})\s+(?P<swimming_pool>\d{2}.{2}[ME])(\s+(?P<inscription_time_date>\d{2}/\d{2}/\d{4})\s+(?P<inscription_time_location>.*))?')
        self.event_data = None
        self.championship_date = championship_date
        self.championship_data = None

    def _process_line(self, new_line):
        match = self.event_re.match(new_line)
        if self.championship_data is None and 'CTO. ESPAÑA' in new_line:
            self.championship_data = {
                'name': new_line.strip().split(',')[0],
                'date': self.championship_date
            }
            return
        if match:
            match_event_dict = match.groupdict()
            self.event_data = {
                'event_number': int(match_event_dict['event_number']),
                'distance': int(match_event_dict['distance']),
                'style': match_event_dict['style'].upper(),
                'gender': match_event_dict['gender'].upper(),
                'category': None,
                'type': 'individual' if match_event_dict['relay'] is None else 'relay'
            }
            return
        elif self.event_data is not None:
            match = self.participant_re.match(new_line)
            if match:
                participant_data = {
                    'license': match.group('license'),
                    'name': match.group('name').upper().strip(),
                }
                if self.event_data['type'] == 'individual':
                    participant_data['born_year'] = int(match.group('year'))
                    self.event_data['category'] = self.get_category_from_born_year(participant_data['born_year'], self.championship_data['date'])
                else:
                    self.event_data['category'] = match.group('year')

                inscription_time_date = match.group('inscription_time_date')
                inscription_time_date = inscription_time_date.strip() if inscription_time_date else '01/01/1900'
                inscription_time_location = match.group('inscription_time_location')
                inscription_time_location = inscription_time_location.strip() if inscription_time_location else ''

                inscription_time_data = {
                    'time': self.process_inscription_time(
                        match.group('inscription_time').lstrip('0').lstrip(':').strip()
                    ),
                    'pool_length': int(match.group('swimming_pool')[0:2]),
                    'chronometer_type': match.group('swimming_pool').strip()[-1].upper(),
                    'date': datetime.strptime(inscription_time_date, '%d/%m/%Y'),
                    'location': inscription_time_location
                }
                data = {
                    'event': self.event_data,
                    'participant': participant_data,
                    'inscription_time': inscription_time_data,
                    'championship': self.championship_data,
                    'heat': 0,
                    'lane': 0,
                    'session': 0,
                    'club': match.group('club').upper().strip(),
                    'initial_position': int(match.group('initial_position'))
                }
                ConversorTiempos.convertir_a_tiempo_database(data)
                self._set_data(data)
                return

    @staticmethod
    def get_category_from_born_year(born_year, competition_date):
        age = 5 * int((competition_date.year - born_year) / 5)
        return str(age)


db = get_inscriptions_db(mode='remote_rest')

championship_list = [
                     ['inscripciones_totales_palma_2014.txt', '20/02/2014'],
                     ['inscripciones_totales_jerez_2014.txt', '27/06/2014'],
                     ['inscripciones_totales_pontevedra_2015.txt', '19/02/2015'],
                     ['inscripciones_totales_zaragoza_2015.txt', '23/07/2015'],
                     ['inscripciones_totales_gijon_2016.txt', '28/01/2016'],
                     ['inscripciones_totales_las_palmas_2016.txt', '30/06/2016'],
                     ['inscripciones_totales_palma_2017.txt', '02/02/2017'],
                     ['inscripciones_totales_badajoz_2017.txt', '29/06/2017'],
                    ]

for champ in championship_list:
    champParser = LegacyChampioshipFileParser(champ[0], datetime.strptime(champ[1], '%d/%m/%Y'))
    for data in champParser:
        # print data
        db.insert(data)
